package de.Lukas.IT.Table;

import java.util.ArrayList;
import java.util.List;

public class TableObject {

private String itemName;
private double price;
private int quantity;
private int id;
private  Boolean firstHead = true;

public TableObject(String itemName, double price, int quantity, int id) {
    this.setItemName(itemName);
    this.setPrice(price);
    this.setQuantity(quantity);
    this.setID(id);
}

public TableObject(){
   
}

public String getItemName() {
    return itemName;
}

public void setItemName(String itemName) {
    this.itemName = itemName;
}

public double getPrice() {
    return price;
}

public void setPrice(double price) {
    this.price = price;
}

public int getQuantity() {
    return quantity;
}

public void setQuantity(int quantity) {
    this.quantity = quantity;
}

private void setID(int id){
    this.id = id;
}
private int getId(){
    return this.id;
}


private static void printInvoiceHeader() {
    System.out.println(String.format("%30s %25s %10s %25s", "Fahrenheit", "|", "Celcius", "|"));
    System.out.println(String.format("%s", "---------------------------------------------------------------------------------------------"));
}
private void printInvoice() {
    System.out.println(String.format("%30s %25s %10.2f %25s", this.getItemName(), "|", this.getPrice(), "|"));
}

private static List<TableObject> buildInvoice(String name, double price, int ammount, int idNum) {
    List<TableObject> itemList = new ArrayList<>();
    itemList.add(new TableObject(name, price, ammount, idNum));
    return itemList;
   
}

public void printTable(String name, double price, int ammount, int idNum){
    if(firstHead){
        TableObject.printInvoiceHeader();
        firstHead = false;
    }
   
    TableObject.buildInvoice(name, price, ammount, idNum).forEach(TableObject::printInvoice);
   
}
}
