package de.Lukas.IT.Fahrkartenautomat;


import java.util.Scanner;

public class Utils {
    public static final int RESTART_CODE = 0;

	public static double calculate(int amount, double price, Scanner key) {
		 double zuZahlenderBetrag; 
	     double eingezahlterGesamtbetrag;
	     double eingeworfeneMünze;
	     double rückgabebetrag;
	     
		 zuZahlenderBetrag = amount * price;

	       // Geldeinwurf
	       // -----------
	       eingezahlterGesamtbetrag = 0.0;
	     
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
	    	   double euro = zuZahlenderBetrag - eingezahlterGesamtbetrag;
	           
	           System.out.printf("Noch zu Zahlen: %.2f", euro).print(" Euro \n");
	           System.out.print("Eingabe: ");
	           eingeworfeneMünze = key.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       
	       }

	       // Fahrscheinausgabe 
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("----");
	          try {
	            Thread.sleep(50);
	        } catch (InterruptedException e) {
	            // TODO Auto-generated catch block 
	            e.printStackTrace();
	        }
	       }
	       System.out.println("\n\n");

	       // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       
	   
		return rückgabebetrag;
	       
	}
	
	  public static void backMoney(double rückgabebetrag) {
	       if(rückgabebetrag > 0.0)
	       {
	           System.out.printf("Der Rückgabebetrag in Höhe von %.2f ", rückgabebetrag).print(" Euro \n");

	           System.out.println("wird in folgenden Münzen ausgezahlt: ");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	              System.out.println("2 EURO");
	              rückgabebetrag -= 2.0;
	              // rückgabebetrag = rückgabebetrag - 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	              System.out.println("1 EURO");
	              rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	              System.out.println("50 CENT");
	              rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	              System.out.println("20 CENT");
	               rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	              System.out.println("10 CENT");
	              rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	              System.out.println("5 CENT");
	               rückgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	    }
	
	  
	  public static boolean checkNumber(String s, boolean intOrDouble) {
		        boolean numeric = true;

		        if (intOrDouble) {
		        try {
		            Double num = Double.parseDouble(s);
		            //System.out.print(num.doubleValue() + "");
		        } catch (NumberFormatException e) {
		            numeric = false;
		        }
		        } else {
		        	try {
			            int num = Integer.parseInt(s);
			            //System.out.print(num.doubleValue() + "");
			        } catch (NumberFormatException e) {
			            numeric = false;
			        }
		        }
		        return numeric;
	  }
	  
		        	

	  
	     
}
