package de.Lukas.IT.Fahrkartenautomat;


import java.util.Scanner;

public class Fahrkartenautomat {

	private double price;
	private int amount;
	private Scanner key;
	
	public Fahrkartenautomat(double price, int amount, Scanner key) {
		this.setPrice(price);
		this.amount = amount;
		this.key = key;
		start();
	}
	
	
	
	public void start() {
		 System.out.print("Bitte geben Sie einen Preis an: ");
		 String checkPrice = key.next();
				 
		 if (Utils.checkNumber(checkPrice, true)) {
			 setPrice(Double.valueOf(checkPrice));
		 } else {
			 price = 1.0;
			 System.out.print("Aufgrunddessen das Sie keinen Preis gewählt haben wird der Preis auf 1 Euro Gesetzt \n");
		 }
		 if (price < 0.0 || price == 0.0 || price == 0 || price < 0) {
			 price = 1.0;
			 System.out.print("Aufgrunddessen das Sie keinen Preis gewählt haben wird der Preis auf 1 Euro Gesetzt \n");
		 }

		 System.out.print("Bitte geben Sie die Anzahl an: ");
		 String checkAmount = key.next();
				 
		 if (Utils.checkNumber(checkAmount, false)) {
			 setAmount(Integer.valueOf(checkAmount));
		 } else {
			 amount = 1;
			 System.out.print("Aufgrunddessen das Sie keine Anzahl gewählt haben wird die Anzahl auf 1 Ticket Gesetzt \n");
		 }
		 if (amount < 0.0 || amount == 0.0 || amount == 0 || amount < 0) {
			 amount = 1;
			 System.out.print("Aufgrunddessen das Sie keine Anzahl gewählt haben wird die Anzahl auf 1 Ticket Gesetzt \n");
		 }

		 

		
		 Utils.backMoney(Utils.calculate(amount, price, key));
		 
	}
	public double getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
